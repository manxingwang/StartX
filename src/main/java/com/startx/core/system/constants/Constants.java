package com.startx.core.system.constants;

public class Constants {
	
	/**
	 * uri
	 */
	public static final String ROOT_DIR = "/";
	public static final String FAVICON = "/favicon.ico";
	public static final String PARAM_SPLIT = "?";
	public static final String EMPTY_STRING = "";
	/**
	 * 冒号
	 */
	public final static String COLON = ":";
	
}
