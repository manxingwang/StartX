package com.startx.core.system.constants;

import io.netty.handler.codec.http.DefaultHttpHeaders;

/**
 * 公共header
 */
public class Headers {
	
	private static final DefaultHttpHeaders JSON_HEADERS = new DefaultHttpHeaders();
	
	private static final DefaultHttpHeaders XML_HEADERS = new DefaultHttpHeaders();

	public static final DefaultHttpHeaders RESOURCE_HEADERS = new DefaultHttpHeaders();
	
	public static final DefaultHttpHeaders COMMON_HEADERS = new DefaultHttpHeaders();

	// 静态资源
	static {
		RESOURCE_HEADERS.add("Access-Control-Allow-Origin", "*");
		RESOURCE_HEADERS.add("Cache-Control",
				"no-store, max-age=0, no-cache, must-revalidate, post-check=0, pre-check=0");
		RESOURCE_HEADERS.add("Pragma", "no-cache");
		RESOURCE_HEADERS.add("Accept",
				"text/html,text/css,text/js,application/pdf," + "text/javascript,application/javascript,image/*,"
						+ "application/json,text/plain;q=0.9,application/xhtml+xml,"
						+ "application/xml,video/*,audio/*,aplication/*,*/*;q=0.8");
	}

	/**
	 * json
	 */
	static {
		JSON_HEADERS.add("Access-Control-Allow-Origin", "*");
		JSON_HEADERS.add("Content-Type", "application/json;charset=utf-8");
		JSON_HEADERS.add("Cache-Control", "no-store, max-age=0, no-cache, must-revalidate, post-check=0, pre-check=0");
		JSON_HEADERS.add("Pragma", "no-cache");
	}
	
	/**
	 * xml
	 */
	static {
		XML_HEADERS.add("Access-Control-Allow-Origin", "*");
		XML_HEADERS.add("Content-Type", "text/xml;charset=utf-8");
		XML_HEADERS.add("Cache-Control", "no-store, max-age=0, no-cache, must-revalidate, post-check=0, pre-check=0");
		XML_HEADERS.add("Pragma", "no-cache");
	}
	
	static {
		COMMON_HEADERS.add("Access-Control-Allow-Origin", "*");
		COMMON_HEADERS.add("Content-Type", "text/html;text/html;application/json");
		COMMON_HEADERS.add("Cache-Control", "no-store, max-age=0, no-cache, must-revalidate, post-check=0, pre-check=0");
		COMMON_HEADERS.add("Pragma", "no-cache");
	}

	public static DefaultHttpHeaders getJsonHeader() {
		return JSON_HEADERS;
	}

	public static DefaultHttpHeaders getResourceHeader() {
		return RESOURCE_HEADERS;
	}
	
	public static DefaultHttpHeaders getXmlHeader() {
		return XML_HEADERS;
	}
	
	public static DefaultHttpHeaders getCommonHeader() {
		return COMMON_HEADERS;
	}
}
